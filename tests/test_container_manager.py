from mimetypes import init
import pytest 
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

from oompa.docker_clients.container_manager import ContainerManager

# Mock docker client / python api in order to remove internet dependency
class MockDockerClient:
    def __init__(self):
        self.containers = MockContainersApi()
        self.images = MockImagesApi()

class MockContainersApi:
    def __init__(self):
        self.containers = {}
    def create(self, image, detach=False, tty=False, ports=None, name="AwesomeEuclid", entrypoint='sh'):
        if image not in ["alpine:latest"]:
            raise Exception("Cannot start container")
        else:
            mock_container = MockContainer(name, ports=ports)
            self.containers[name] = mock_container
            return mock_container
    def get(self, name):
        return self.containers[name]

class MockImagesApi:
    def pull(self, name):
        if name not in ["alpine:latest", 'alpine:nostart']:
            raise Exception("repository does not exist or may require 'docker login': denied: requested access to the resource is denied")

class MockContainer:
    def __init__(self, name, ports = None):
        self.name = name
        self.ports = ports
    def start(self):
        self.ports = {'22/tcp': [{'HostPort': 30022}]}
        return True
    def exec_run(self):
        import os
        os.dev
        pass
    def put_archive(self):
        pass
    def commit(self, repository, tag):
        return "repository:tag"

def test_image_pull():
    # Arrange
    client = MockDockerClient()
    container_manager = ContainerManager(client=client)

    # Act
    container_manager.image_pull('alpine:latest')

    # Assert
    assert container_manager.image == 'alpine:latest'

def test_image_pull_fail():
    # Arrange
    client = MockDockerClient()
    container_manager = ContainerManager(client=client)

    # Act
    with pytest.raises(Exception) as e_info:
        container_manager.image_pull("image:bad")

    # Assert
    assert e_info.match("repository does not exist or may require 'docker login': denied: requested access to the resource is denied")

def test_start_container():
    # Arrange
    client = MockDockerClient()
    container_manager = ContainerManager(client=client)
    container_manager.image_pull("alpine:latest")
    container_name = "AwesomeEuclid"

    # Act
    container_manager.start_container(name = container_name)
    container_started = (container_manager.name == container_name)

    # Assert
    assert container_started

def test_start_container_fail():
    # Arrange
    client = MockDockerClient()
    container_manager = ContainerManager(client=client)
    container_manager.image_pull("alpine:nostart")

    # Act
    with pytest.raises(Exception) as e_info:
        container_manager.start_container()

    # Assert
    assert e_info.match("Cannot start container")

def test_get_port():
    # Arrange
    client = MockDockerClient()
    container_manager = ContainerManager(client=client)
    container_manager.image_pull("alpine:latest")
    container_name = "AwesomeEuclid"
    container_manager.start_container(name = container_name)

    # Act
    port = container_manager.get_port()

    # Assert
    assert port==30022

def test_save(capsys):
    # Arrange
    client = MockDockerClient()
    container_manager = ContainerManager(client=client)
    container_manager.image_pull("alpine:latest")
    container_name = "AwesomeEuclid"
    container_manager.start_container(name = container_name)

    # Act
    container_manager.save("test", "latest")
    captured = capsys.readouterr()

    # Assert
    assert "Successfully created new image test:latest" in captured.out

# Need to think of how to test exec_run 
