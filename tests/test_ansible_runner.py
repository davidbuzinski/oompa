import pytest 
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

from oompa.ansible_clients.ansible_runner import AnsibleRunner

def test_update_port():
    # Arrange
    runner = AnsibleRunner()

    # Act
    runner.update_port(32222)

    # Assert
    assert runner.port == 32222

def test_absolute_roles_path():
    full_path = os.getcwd()+ '/' + './roles-test'
    runner = AnsibleRunner(roles_directory=full_path)
    assert runner.roles_path == full_path


def test_relative_roles_path():
    runner = AnsibleRunner(roles_directory='./roles-test') 
    full_path = os.getcwd()+ '/' + './roles-test'
    assert runner.roles_path == full_path

def test_load_roles():
    # Arrange 
    runner = AnsibleRunner()

    # Act
    runner.load_roles([{'name': 'dbuzinski.dhall'}])

    # Assert
    play = runner.play_list[0] # get only play in play_list
    task_action = play.get('tasks', [])
    assert task_action == [{'name': 'include role dbuzinski.dhall', 'include_role': {'name': 'dbuzinski.dhall'}}]
    
def test_add_play():
    # Arrange 
    import yaml

    runner = AnsibleRunner()
    with open('./tests/playbook-test.yml', 'r') as f:
        playbook = yaml.safe_load(f)

    # Act
    runner.add_play(playbook)

    # Assert
    assert runner.play_list == [{'name': 'Ping playbook for testing purposes','hosts': 'localhost' ,'tasks': [{'name': 'Ping test', 'ping': None}]}]

def test_execute_playbook(capsys):
    # Arrange 
    import yaml 
    runner = AnsibleRunner()
    with open('./tests/playbook-test.yml', 'r') as f:
        playbook = yaml.safe_load(f)

    runner.add_play(playbook)

    # Act
    runner.execute_playbook(connection='local')
    captured = capsys.readouterr()

    # Assert
    assert "TASK [Ping test] ***************************************************************" in captured.out


