from csv import excel
import pytest 
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

from oompa.file_parser.file_parser import FileParser

def test_set_file_dhall():
    parser = FileParser()
    parser.set_file("./tests/builder.dhall")
    assert parser.format == 'DHALL'

def test_set_file_yaml():
    parser = FileParser()
    parser.set_file("./tests/builder.yaml")
    assert parser.format == 'YAML'

def test_set_file_yml():
    parser = FileParser()
    parser.set_file("./tests/builder.yml")
    assert parser.format == 'YAML'

def test_set_file_json():
    parser = FileParser()
    parser.set_file("./tests/builder.json")
    assert parser.format == 'JSON'

def test_set_file_bad():
    parser = FileParser()
    with pytest.raises(Exception) as e_info:
        parser.set_file("./tests/builder.bad")
    assert e_info.match("Unacceptable file format ./tests/builder.bad. Acceptable formats are: .dhall, .yml, .yaml, .json")

def test_set_relative_path():
    import os
    parser = FileParser()
    cwd = os.getcwd() + "/"
    absolute_path = cwd + "./tests/"
    parser.set_path("./tests/")
    assert parser.path == absolute_path

def test_set_absolute_path():
    import os
    parser = FileParser()
    cwd = os.getcwd() + "/"
    absolute_path = cwd + "tests/"
    parser.set_path(absolute_path)
    assert parser.path == absolute_path

def test_parse_file_dhall():
    parser = FileParser()
    parser.set_file("./tests/builder.dhall")
    config = parser.parse_file()
    assert "srcImage" in config
    assert "destImage" in config
    assert "rolesList" in config

def test_parse_file_json():
    parser = FileParser()
    parser.set_file("./tests/builder.json")
    config = parser.parse_file()
    assert "srcImage" in config
    assert "destImage" in config
    assert "rolesList" in config

def test_parse_file_yaml():
    parser = FileParser()
    parser.set_file("./tests/builder.yaml")
    config = parser.parse_file()
    assert "srcImage" in config
    assert "destImage" in config
    assert "rolesList" in config

def test_parse_file_yml():
    parser = FileParser()
    parser.set_file("./tests/builder.yml")
    config = parser.parse_file()
    assert "srcImage" in config
    assert "destImage" in config
    assert "rolesList" in config

def test_set_relative_path_path_parse_file_dhall():
    parser = FileParser()
    parser.set_path("./tests")
    parser.set_file("builder.dhall")
    config = parser.parse_file()
    assert "srcImage" in config
    assert "destImage" in config
    assert "rolesList" in config

def test_set_relative_path_path_parse_file_json():
    parser = FileParser()
    parser.set_path("./tests")
    parser.set_file("builder.json")
    config = parser.parse_file()
    assert "srcImage" in config
    assert "destImage" in config
    assert "rolesList" in config

def test_set_relative_path_path_parse_file_yaml():
    parser = FileParser()
    parser.set_path("./tests")
    parser.set_file("builder.yaml")
    config = parser.parse_file()
    assert "srcImage" in config
    assert "destImage" in config
    assert "rolesList" in config

def test_set_relative_path_parse_file_yml():
    parser = FileParser()
    parser.set_path("./tests")
    parser.set_file("builder.yml")
    config = parser.parse_file()
    assert "srcImage" in config
    assert "destImage" in config
    assert "rolesList" in config

def test_set_relative_path_parse_no_file():
    parser = FileParser()
    parser.set_path("./tests")
    config = parser.parse_file()
    assert "srcImage" in config
    assert "destImage" in config
    assert "rolesList" in config

def test_set_absolute_path_path_parse_file_dhall():
    import os
    parser = FileParser()
    cwd = os.getcwd() + "/"
    absolute_path = cwd + "tests/"
    parser = FileParser()
    parser.set_path(absolute_path)
    parser.set_file("builder.dhall")
    config = parser.parse_file()
    assert "srcImage" in config
    assert "destImage" in config
    assert "rolesList" in config

def test_set_absolute_path_path_parse_file_json():
    import os
    parser = FileParser()
    cwd = os.getcwd() + "/"
    absolute_path = cwd + "tests/"
    parser = FileParser()
    parser.set_path(absolute_path)
    parser.set_file("builder.json")
    config = parser.parse_file()
    assert "srcImage" in config
    assert "destImage" in config
    assert "rolesList" in config

def test_set_absolute_path_path_parse_file_yaml():
    import os
    parser = FileParser()
    cwd = os.getcwd() + "/"
    absolute_path = cwd + "tests/"
    parser = FileParser()
    parser.set_path(absolute_path)
    parser.set_file("builder.yaml")
    config = parser.parse_file()
    assert "srcImage" in config
    assert "destImage" in config
    assert "rolesList" in config

def test_set_absolute_path_parse_file_yml():
    import os
    parser = FileParser()
    cwd = os.getcwd() + "/"
    absolute_path = cwd + "tests/"
    parser = FileParser()
    parser.set_path(absolute_path)
    parser.set_file("builder.yml")
    config = parser.parse_file()
    assert "srcImage" in config
    assert "destImage" in config
    assert "rolesList" in config

def test_set_absolute_path_parse_no_file():
    import os
    parser = FileParser()
    cwd = os.getcwd() + "/"
    absolute_path = cwd + "tests/"
    parser = FileParser()
    parser.set_path(absolute_path)
    config = parser.parse_file()
    assert "srcImage" in config
    assert "destImage" in config
    assert "rolesList" in config
