import pytest 
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

from oompa.ansible_clients.roles_installer import RolesInstaller

def test_parse_role():
    # Arrange
    roles_installer = RolesInstaller()

    # Act
    parsed_role = roles_installer.parse_role('test:1.0.0')
    # Assert
    assert parsed_role == {'name': 'test', 'version': '1.0.0'}

def test_parse_role_no_tag():
    # Arrange
    roles_installer = RolesInstaller()

    # Act
    parsed_role = roles_installer.parse_role('test')
    # Assert
    assert parsed_role == {'name': 'test'}

def test_parse_role():
    # Arrange
    roles_installer = RolesInstaller()

    # Act
    parsed_role = roles_installer.parse_role('test:1.0.0')
    # Assert
    assert parsed_role.get('name') == 'test'
    assert parsed_role.get('version') == '1.0.0'

def test_role_installed():
    roles_installer = RolesInstaller(roles_directory='./tests')
    installed = roles_installer.role_installed({'name': './test-role'})
    assert installed == True

def test_role_not_installed():
    roles_installer = RolesInstaller(roles_directory='./tests')
    installed = roles_installer.role_installed({'name': './not-installed-test-role'})
    assert installed == False

def test_install_roles():
    roles_installer = RolesInstaller(roles_directory='./tests')
    installed = roles_installer.install_roles(['test-role'])
    assert 1 == 1
