let Config : Type =
    { srcImage  : Text
    , destImage : Text
    , rolesList : List Text
    }

let config : Config = 
    { srcImage      = "centos:7"
    , destImage     = "dhall:1.38.1"
    , rolesList     = ["dbuzinski.dhall:1.0.0"]
    }

in config
