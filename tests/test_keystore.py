import pytest 
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

from oompa.utils.keystore import Keystore

def test_create_private_key_file_exists():
    # Cleanup
    if os.path.exists("./.oompa_key"):
        os.remove("./.oompa_key")
    # Test
    keystore = Keystore()
    keystore.create_private_key_file()
    assert os.path.exists("./.oompa_key")

def test_create_public_key_file_exists():
    # Cleanup
    if os.path.exists("./.oompa_key.pub"):
        os.remove("./.oompa_key.pub")
    # Test
    keystore = Keystore()
    keystore.create_public_key_file()
    assert os.path.exists("./.oompa_key.pub")

def test_create_private_key_file_is_key():
    # Cleanup
    if os.path.exists("./.oompa_key"):
        os.remove("./.oompa_key")
    # Test
    keystore = Keystore()
    keystore.create_private_key_file()
    assert 1 == 1

def test_create_public_key_file_is_key():
    # Cleanup
    if os.path.exists("./.oompa_key.pub"):
        os.remove("./.oompa_key.pub")
    # Test
    keystore = Keystore()
    keystore.create_public_key_file()
    assert 1 == 1
