#!/bin/sh -e
#
# Script to prepare Docker images for further configuration with Oompa.
#
# This script is only meant to be run from within Docker images, not on local machines!
#
# Arguments (use `... | sh -s - ARGUMENTS`)
#
# -q: reduce script's output
#
# Note that this script must be run as root.
#
# This script was influenced by https://github.com/commercialhaskell/stack/blob/master/etc/scripts/get-stack.sh
# 

QUIET=""
export DEBIAN_FRONTEND="noninteractive"

# print a message to stderr and exit with error code
die() {
  echo "$@" >&2
  exit 1
}

# print a message to stdout unless '-q' passed to script
info() {
  if [ -z "$QUIET" ] ; then
    echo "$@"
  fi
}

# Check whether 'apt-get' command exists
has_apt_get() {
  has_cmd apt-get
}

# Check whether 'yum' command exists
has_yum() {
  has_cmd yum
}

# Check whether 'apk' command exists
has_apk() {
  has_cmd apk
}

# Check whether the given command exists
has_cmd() {
  command -v "$1" > /dev/null 2>&1
}

has_python() {
  has_cmd python
}

# Attempt to install packages using whichever of apt-get, yum, or apk is
# available.
try_install_pkgs() {
  if has_apt_get ; then
    apt_get_install_pkgs "$@"
  elif has_yum ; then
    yum_install_pkgs "$@"
  elif has_apk ; then
    apk_install_pkgs "$@"
  else
    return 1
  fi
}

install_python() {
  if ! has_python ; then 
    try_install_pkgs python3
  fi
}

# Install packages using apt-get
apt_get_install_pkgs() {
  missing=
  DEBIAN_FRONTEND=noninteractive apt-get update
  for pkg in $*; do
    if ! dpkg -s $pkg 2>/dev/null |grep '^Status:.*installed' >/dev/null; then
      missing="$missing $pkg"
    fi
  done
  if [ "$missing" = "" ]; then
    info "Already installed!"
  elif ! apt-get install -y ${QUIET:+-qq}$missing; then
    info "\nInstalling apt packages failed.  Please run 'apt-get update' and try again."
  fi
}

verify_yum_mirrors() {
  if ! yum list curl ; then 
    sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
    sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-* 
  fi 
}

# Install packages using yum
yum_install_pkgs() {
  verify_yum_mirrors "$@"
  if yum install -y ${QUIET:+-q} "$@"; then
    info "\nInstalling yum packages failed.  Please run 'yum check-update' and try again."
  fi
}

# Install packages using apk
apk_install_pkgs() {
  if ! apk add --update ${QUIET:+-q} "$@"; then
    info "\nInstalling apk packages failed.  Please run 'apk update' and try again."
  fi
}

set_ssh_config() {
    echo 'root:dummy_passwd'|chpasswd
    mkdir -p /.tmp
    mkdir -p /run/sshd
    ssh-keygen -P "" -A 
}

start_ssh_server() {
    /usr/sbin/sshd
}

while [ $# -gt 0 ]; do
  case "$1" in
    -q|--quiet)
      # This tries its best to reduce output by suppressing the script's own
      # messages and passing "quiet" arguments to tools that support them.
      QUIET="true"
      shift
      ;;
    *)
      echo "Invalid argument: $1" >&2
      exit 1
      ;;
  esac
done

try_install_pkgs openssh-server
try_install_pkgs openssh 
try_install_pkgs wget 
install_python

set_ssh_config
start_ssh_server
