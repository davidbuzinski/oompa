# Oompa

Oompa is a tool to build Docker images with an easy to read configuration.
It uses Dhall configuration language and Ansible Galaxy roles to build and configure containers.

## Installation

The latest stable version [is available on PyPI](https://pypi.python.org/pypi/oompa/). Either add `oompa` to your `requirements.txt` file or install with pip:

    pip install oompa

## Use Oompa

Use docs to come soon.

## Get Involved

Contributing instructions to come soon.

## Release Info

Oompa is still in Alpha / development. Bugs are being worked out, new features are being added, and APIs are rapidly changing.

## License

MIT License

